package ro.ase.cts;

public class TestClass {

	public static void main(String[] args) {
		System.out.println("Hello git! The name of the license project is Machine learning in natural language processing");
		Sentiment s1=new Sentiment("positive","bigTrainingData");
		s1.displaySentiment();
		Sentiment s2=new Sentiment("negative","smallTrainingData");
		s2.displaySentiment();
	}

}
