package ro.ase.cts;

public class Sentiment implements SentimentInterface {
	private String commentToAnalyze;
	private String trainingData;
	public Sentiment(String commentToAnalyze, String trainingData) {
		super();
		this.commentToAnalyze = commentToAnalyze;
		this.trainingData = trainingData;
	}
	
	
	public String getCommentToAnalyze() {
		return commentToAnalyze;
	}


	public void setCommentToAnalyze(String commentToAnalyze) {
		this.commentToAnalyze = commentToAnalyze;
	}


	public String getTrainingData() {
		return trainingData;
	}


	public void setTrainingData(String trainingData) {
		this.trainingData = trainingData;
	}


	@Override
	public void displaySentiment() {
		if(this.commentToAnalyze=="positive") {
			System.out.println("Sentiment was positive");
		}
		else {
			System.out.println("Sentiment was negative");
		}
		
	}
	
	
	
}
